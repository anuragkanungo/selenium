package com.example.tests;

import com.thoughtworks.selenium.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class AppTest {
	private Selenium selenium;

	@Before
	public void setUp() throws Exception {
		selenium = new DefaultSelenium("172.31.63.204", 4443, "*firefox", "http://172.31.60.246:8080");
		selenium.start();
	}

	@Test
	public void testPetclinic() throws Exception {
		selenium.open("/petclinic/");
		selenium.click("link=Home");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Find owners");
		selenium.waitForPageToLoad("30000");
		selenium.type("name=lastName", "Anurag");
		selenium.click("css=button[type=\"submit\"]");
		selenium.waitForPageToLoad("30000");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
